﻿namespace EstructuraDeDatos
{
    partial class FrmSumaMatrices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRenglones = new System.Windows.Forms.Label();
            this.lblColumnas = new System.Windows.Forms.Label();
            this.lblInferior = new System.Windows.Forms.Label();
            this.lblSuperior = new System.Windows.Forms.Label();
            this.txtRenglones = new System.Windows.Forms.TextBox();
            this.txtColumnas = new System.Windows.Forms.TextBox();
            this.txtLimInf = new System.Windows.Forms.TextBox();
            this.txtLimSup = new System.Windows.Forms.TextBox();
            this.btnGenerar = new System.Windows.Forms.Button();
            this.btnRandom = new System.Windows.Forms.Button();
            this.btnSuma = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRenglones
            // 
            this.lblRenglones.AutoSize = true;
            this.lblRenglones.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRenglones.Location = new System.Drawing.Point(49, 32);
            this.lblRenglones.Name = "lblRenglones";
            this.lblRenglones.Size = new System.Drawing.Size(115, 25);
            this.lblRenglones.TabIndex = 0;
            this.lblRenglones.Text = "Renglones";
            // 
            // lblColumnas
            // 
            this.lblColumnas.AutoSize = true;
            this.lblColumnas.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblColumnas.Location = new System.Drawing.Point(49, 91);
            this.lblColumnas.Name = "lblColumnas";
            this.lblColumnas.Size = new System.Drawing.Size(108, 25);
            this.lblColumnas.TabIndex = 1;
            this.lblColumnas.Text = "Columnas";
            // 
            // lblInferior
            // 
            this.lblInferior.AutoSize = true;
            this.lblInferior.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInferior.Location = new System.Drawing.Point(49, 344);
            this.lblInferior.Name = "lblInferior";
            this.lblInferior.Size = new System.Drawing.Size(141, 25);
            this.lblInferior.TabIndex = 2;
            this.lblInferior.Text = "Limite inferior";
            // 
            // lblSuperior
            // 
            this.lblSuperior.AutoSize = true;
            this.lblSuperior.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSuperior.Location = new System.Drawing.Point(49, 405);
            this.lblSuperior.Name = "lblSuperior";
            this.lblSuperior.Size = new System.Drawing.Size(153, 25);
            this.lblSuperior.TabIndex = 3;
            this.lblSuperior.Text = "Limite superior";
            // 
            // txtRenglones
            // 
            this.txtRenglones.Location = new System.Drawing.Point(214, 37);
            this.txtRenglones.Name = "txtRenglones";
            this.txtRenglones.Size = new System.Drawing.Size(120, 20);
            this.txtRenglones.TabIndex = 4;
            this.txtRenglones.Text = "7";
            // 
            // txtColumnas
            // 
            this.txtColumnas.Location = new System.Drawing.Point(214, 96);
            this.txtColumnas.Name = "txtColumnas";
            this.txtColumnas.Size = new System.Drawing.Size(120, 20);
            this.txtColumnas.TabIndex = 5;
            this.txtColumnas.Text = "3";
            // 
            // txtLimInf
            // 
            this.txtLimInf.Location = new System.Drawing.Point(214, 344);
            this.txtLimInf.Name = "txtLimInf";
            this.txtLimInf.Size = new System.Drawing.Size(120, 20);
            this.txtLimInf.TabIndex = 6;
            this.txtLimInf.Text = "-100";
            // 
            // txtLimSup
            // 
            this.txtLimSup.Location = new System.Drawing.Point(214, 410);
            this.txtLimSup.Name = "txtLimSup";
            this.txtLimSup.Size = new System.Drawing.Size(120, 20);
            this.txtLimSup.TabIndex = 7;
            this.txtLimSup.Text = "100";
            // 
            // btnGenerar
            // 
            this.btnGenerar.Location = new System.Drawing.Point(395, 63);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(75, 23);
            this.btnGenerar.TabIndex = 8;
            this.btnGenerar.Text = "Generar";
            this.btnGenerar.UseVisualStyleBackColor = true;
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // btnRandom
            // 
            this.btnRandom.Location = new System.Drawing.Point(395, 380);
            this.btnRandom.Name = "btnRandom";
            this.btnRandom.Size = new System.Drawing.Size(75, 23);
            this.btnRandom.TabIndex = 9;
            this.btnRandom.Text = "Random";
            this.btnRandom.UseVisualStyleBackColor = true;
            this.btnRandom.Click += new System.EventHandler(this.btnRandom_Click);
            // 
            // btnSuma
            // 
            this.btnSuma.Location = new System.Drawing.Point(533, 380);
            this.btnSuma.Name = "btnSuma";
            this.btnSuma.Size = new System.Drawing.Size(75, 23);
            this.btnSuma.TabIndex = 10;
            this.btnSuma.Text = "Suma";
            this.btnSuma.UseVisualStyleBackColor = true;
            this.btnSuma.Click += new System.EventHandler(this.btnSuma_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 159);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 150);
            this.dataGridView1.TabIndex = 5;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(280, 159);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(240, 150);
            this.dataGridView2.TabIndex = 6;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(548, 159);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(240, 150);
            this.dataGridView3.TabIndex = 7;
            // 
            // FrmSumaMatrices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnSuma);
            this.Controls.Add(this.btnRandom);
            this.Controls.Add(this.btnGenerar);
            this.Controls.Add(this.txtLimSup);
            this.Controls.Add(this.txtLimInf);
            this.Controls.Add(this.txtColumnas);
            this.Controls.Add(this.txtRenglones);
            this.Controls.Add(this.lblSuperior);
            this.Controls.Add(this.lblInferior);
            this.Controls.Add(this.lblColumnas);
            this.Controls.Add(this.lblRenglones);
            this.Name = "FrmSumaMatrices";
            this.Text = "FrmSumaMatrices";
            this.Load += new System.EventHandler(this.FrmSumaMatrices_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRenglones;
        private System.Windows.Forms.Label lblColumnas;
        private System.Windows.Forms.Label lblInferior;
        private System.Windows.Forms.Label lblSuperior;
        private System.Windows.Forms.TextBox txtRenglones;
        private System.Windows.Forms.TextBox txtColumnas;
        private System.Windows.Forms.TextBox txtLimInf;
        private System.Windows.Forms.TextBox txtLimSup;
        private System.Windows.Forms.Button btnGenerar;
        private System.Windows.Forms.Button btnRandom;
        private System.Windows.Forms.Button btnSuma;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView3;
    }
}