﻿namespace EstructuraDeDatos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Objetivo = new System.Windows.Forms.Label();
            this.Nombre = new System.Windows.Forms.Label();
            this.Descripcion = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.uNIDAD1ConceptosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tiposDeDatosAbstractosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.practica1ADTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recursividadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fibonacciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mCDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.busquedasBinariasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pruebaDeFibonnacciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fractalDeHilbertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uNIDAD2ArreglosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uNIDAD3ListasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uNIDAD4PlasYColasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uNIDAD5ArbolesBinariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.Objetivo);
            this.panel1.Controls.Add(this.Nombre);
            this.panel1.Controls.Add(this.Descripcion);
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(799, 355);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::EstructuraDeDatos.Properties.Resources.secuencia_de_fibonacci_2;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Image = global::EstructuraDeDatos.Properties.Resources.secuencia_de_fibonacci_2;
            this.pictureBox1.Location = new System.Drawing.Point(455, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(312, 217);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // Objetivo
            // 
            this.Objetivo.AutoSize = true;
            this.Objetivo.Location = new System.Drawing.Point(51, 179);
            this.Objetivo.Name = "Objetivo";
            this.Objetivo.Size = new System.Drawing.Size(35, 13);
            this.Objetivo.TabIndex = 3;
            this.Objetivo.Text = "label1";
            // 
            // Nombre
            // 
            this.Nombre.AutoSize = true;
            this.Nombre.ForeColor = System.Drawing.SystemColors.Desktop;
            this.Nombre.Location = new System.Drawing.Point(31, 106);
            this.Nombre.Name = "Nombre";
            this.Nombre.Size = new System.Drawing.Size(104, 13);
            this.Nombre.TabIndex = 2;
            this.Nombre.Text = "Bruno Cordova Avila";
            // 
            // Descripcion
            // 
            this.Descripcion.AutoSize = true;
            this.Descripcion.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Descripcion.Location = new System.Drawing.Point(31, 48);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(238, 13);
            this.Descripcion.TabIndex = 1;
            this.Descripcion.Text = "Muestra practica de estructura de datos aplicada";
            this.Descripcion.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uNIDAD1ConceptosToolStripMenuItem,
            this.uNIDAD2ArreglosToolStripMenuItem,
            this.uNIDAD3ListasToolStripMenuItem,
            this.uNIDAD4PlasYColasToolStripMenuItem,
            this.uNIDAD5ArbolesBinariosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(799, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // uNIDAD1ConceptosToolStripMenuItem
            // 
            this.uNIDAD1ConceptosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tiposDeDatosAbstractosToolStripMenuItem,
            this.recursividadToolStripMenuItem});
            this.uNIDAD1ConceptosToolStripMenuItem.Name = "uNIDAD1ConceptosToolStripMenuItem";
            this.uNIDAD1ConceptosToolStripMenuItem.Size = new System.Drawing.Size(177, 20);
            this.uNIDAD1ConceptosToolStripMenuItem.Text = "UNIDAD 1: Conceptos basicos";
            // 
            // tiposDeDatosAbstractosToolStripMenuItem
            // 
            this.tiposDeDatosAbstractosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.practica1ADTToolStripMenuItem});
            this.tiposDeDatosAbstractosToolStripMenuItem.Name = "tiposDeDatosAbstractosToolStripMenuItem";
            this.tiposDeDatosAbstractosToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.tiposDeDatosAbstractosToolStripMenuItem.Text = "Tipos de datos abstractos";
            // 
            // practica1ADTToolStripMenuItem
            // 
            this.practica1ADTToolStripMenuItem.Name = "practica1ADTToolStripMenuItem";
            this.practica1ADTToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.practica1ADTToolStripMenuItem.Text = "Practica 1 ADT";
            this.practica1ADTToolStripMenuItem.Click += new System.EventHandler(this.practica1ADTToolStripMenuItem_Click);
            // 
            // recursividadToolStripMenuItem
            // 
            this.recursividadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fibonacciToolStripMenuItem,
            this.mCDToolStripMenuItem,
            this.busquedasBinariasToolStripMenuItem,
            this.pruebaDeFibonnacciToolStripMenuItem,
            this.fractalDeHilbertToolStripMenuItem});
            this.recursividadToolStripMenuItem.Name = "recursividadToolStripMenuItem";
            this.recursividadToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.recursividadToolStripMenuItem.Text = "Recursividad";
            // 
            // fibonacciToolStripMenuItem
            // 
            this.fibonacciToolStripMenuItem.Name = "fibonacciToolStripMenuItem";
            this.fibonacciToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.fibonacciToolStripMenuItem.Text = "Fibonacci";
            this.fibonacciToolStripMenuItem.Click += new System.EventHandler(this.fibonacciToolStripMenuItem_Click);
            // 
            // mCDToolStripMenuItem
            // 
            this.mCDToolStripMenuItem.Name = "mCDToolStripMenuItem";
            this.mCDToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.mCDToolStripMenuItem.Text = "MCD";
            // 
            // busquedasBinariasToolStripMenuItem
            // 
            this.busquedasBinariasToolStripMenuItem.Name = "busquedasBinariasToolStripMenuItem";
            this.busquedasBinariasToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.busquedasBinariasToolStripMenuItem.Text = "Busquedas binarias";
            // 
            // pruebaDeFibonnacciToolStripMenuItem
            // 
            this.pruebaDeFibonnacciToolStripMenuItem.Name = "pruebaDeFibonnacciToolStripMenuItem";
            this.pruebaDeFibonnacciToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.pruebaDeFibonnacciToolStripMenuItem.Text = "Prueba de Fibonnacci";
            // 
            // fractalDeHilbertToolStripMenuItem
            // 
            this.fractalDeHilbertToolStripMenuItem.Name = "fractalDeHilbertToolStripMenuItem";
            this.fractalDeHilbertToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.fractalDeHilbertToolStripMenuItem.Text = "Fractal de hilbert";
            // 
            // uNIDAD2ArreglosToolStripMenuItem
            // 
            this.uNIDAD2ArreglosToolStripMenuItem.Name = "uNIDAD2ArreglosToolStripMenuItem";
            this.uNIDAD2ArreglosToolStripMenuItem.Size = new System.Drawing.Size(122, 20);
            this.uNIDAD2ArreglosToolStripMenuItem.Text = "UNIDAD 2: Arreglos";
            // 
            // uNIDAD3ListasToolStripMenuItem
            // 
            this.uNIDAD3ListasToolStripMenuItem.Name = "uNIDAD3ListasToolStripMenuItem";
            this.uNIDAD3ListasToolStripMenuItem.Size = new System.Drawing.Size(107, 20);
            this.uNIDAD3ListasToolStripMenuItem.Text = "UNIDAD 3: Listas";
            // 
            // uNIDAD4PlasYColasToolStripMenuItem
            // 
            this.uNIDAD4PlasYColasToolStripMenuItem.Name = "uNIDAD4PlasYColasToolStripMenuItem";
            this.uNIDAD4PlasYColasToolStripMenuItem.Size = new System.Drawing.Size(143, 20);
            this.uNIDAD4PlasYColasToolStripMenuItem.Text = "UNIDAD 4: Pilas y Colas";
            // 
            // uNIDAD5ArbolesBinariosToolStripMenuItem
            // 
            this.uNIDAD5ArbolesBinariosToolStripMenuItem.Name = "uNIDAD5ArbolesBinariosToolStripMenuItem";
            this.uNIDAD5ArbolesBinariosToolStripMenuItem.Size = new System.Drawing.Size(163, 20);
            this.uNIDAD5ArbolesBinariosToolStripMenuItem.Text = "UNIDAD: 5 Arboles Binarios";
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(1, 352);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(798, 98);
            this.panel2.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estructura de datos aplicada";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem uNIDAD1ConceptosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uNIDAD2ArreglosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uNIDAD3ListasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uNIDAD4PlasYColasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uNIDAD5ArbolesBinariosToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label Objetivo;
        private System.Windows.Forms.Label Nombre;
        private System.Windows.Forms.Label Descripcion;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripMenuItem tiposDeDatosAbstractosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem practica1ADTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recursividadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fibonacciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mCDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem busquedasBinariasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pruebaDeFibonnacciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fractalDeHilbertToolStripMenuItem;
    }
}

