﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EstructuraDeDatos
{

    public partial class TorresDeHanoi : Form
    {
        // aqui se definen las variables porque el prof Abril dice
        private String origen = "";
        private int contador = 0;
        private String win = "GANASTE COMO DEBIL!!!!";
        private String win2 = "GANASTE!!!!";
        private Stack<PictureBox> pilaPicBox1 = new Stack<PictureBox>();
        private Stack<PictureBox> pilaPicBox2 = new Stack<PictureBox>();
        private Stack<PictureBox> pilaPicBox3 = new Stack<PictureBox>();
        public TorresDeHanoi()         
        {
            InitializeComponent();
            foreach (Panel item in this.Controls.OfType<Panel>())
            {
                //MessageBox.Show(item.Name); //este messagebox muestra los paneles que hay
                item.AllowDrop = true;
                item.DragEnter += Panel_DragEnter;
                item.DragDrop += Panel_DragDrop;
            }
            pictureBox3.MouseDown += pictureBox3_MouseDown;
            pictureBox4.MouseDown += pictureBox4_MouseDown;
            pictureBox5.MouseDown += pictureBox5_MouseDown;
        }

        private bool setPosition(PictureBox picture, string origen)
        {
            //MessageBox.Show(origen + " " + picture.Parent.Name.ToString());
            switch (picture.Parent.Name.ToString())
            {
                case "panel1":
                    if (origen == "panel2") return validador(pilaPicBox2, pilaPicBox1, picture);

                    if (origen == "panel3") return validador(pilaPicBox3, pilaPicBox1, picture);

                    break;

                case "panel2":
                    if (origen == "panel1") return validador(pilaPicBox1, pilaPicBox2, picture);

                    if (origen == "panel3") return validador(pilaPicBox3, pilaPicBox2, picture);
                    break;

                case "panel3":
                    if (origen == "panel1") return validador(pilaPicBox1, pilaPicBox3, picture);

                    if (origen == "panel2") return validador(pilaPicBox2, pilaPicBox3, picture);
                    break;

            }
            return true;
        }
        private void Panel_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }
        private void Panel_DragDrop(object sender, DragEventArgs e)
        {
            ((PictureBox)e.Data.GetData(typeof(PictureBox))).Parent = (Panel)sender;
        }

        private void pictureBox3_MouseDown(object sender, MouseEventArgs e)
        {
            Mover(pictureBox3);
        }

        private void pictureBox4_MouseDown(object sender, MouseEventArgs e)
        {
            Mover(pictureBox4);
        }

        private void pictureBox5_MouseDown(object sender, MouseEventArgs e)
        {
            Mover(pictureBox5);
        }

        private void Mover (PictureBox pictureBox)
        {
            
            origen = pictureBox.Parent.Name.ToString();
            //MessageBox.Show(origen);
            pictureBox.DoDragDrop(pictureBox, DragDropEffects.Move);
            if (!setPosition(pictureBox, origen))
            {
                switch (origen)
                {
                    case "panel1":
                        pictureBox.Parent = panel1;
                        break;

                    case "panel2":
                        pictureBox.Parent = panel2;
                        break;

                    case "panel3":
                        pictureBox.Parent = panel3;
                        break;

                }
            }

            pictureBox.BringToFront();

        }

        private void TorresDeHanoi_Load(object sender, EventArgs e)
        {
            pictureBox3.Parent = panel1;
            pictureBox4.Parent = panel1;
            pictureBox5.Parent = panel1;

            pictureBox3.BringToFront();
            pictureBox4.BringToFront();
            pictureBox5.BringToFront();

            pilaPicBox1.Clear();
            pilaPicBox2.Clear();
            pilaPicBox3.Clear();

            pilaPicBox1.Push(pictureBox3);
            pilaPicBox1.Push(pictureBox4);
            pilaPicBox1.Push(pictureBox5);
            contador = 0;
            label4.Text = "Movimientos: 0";
        }

        private bool validador(Stack<PictureBox> origen, Stack<PictureBox> destino, PictureBox pictureBox)
        {
            if (
                (destino.Count == 0 && pictureBox.Tag == origen.Peek().Tag)
                ||


                (destino.Count != 0 &&
                int.Parse(destino.Peek().Tag.ToString()) > int.Parse(pictureBox.Tag.ToString())
                && pictureBox.Tag.ToString() == origen.Peek().Tag.ToString())

                )
            {
                destino.Push(origen.Pop());
                //MessageBox.Show(destino.Count.ToString());
                pictureBox.Top = panel1.Height - pictureBox.Height - (destino.Count * pictureBox.Height) ;
                label4.Text = "Movimientos" + " " + (++contador);
                label4.Update();
                if (pilaPicBox3.Count == 3)
                {
                    MessageBox.Show(win2);
                }
                return true;
            }
            else
            {
                return false;
            }
            

        }

        private void btnResolver_Click(object sender, EventArgs e)
        {
            TorresDeHanoi_Load(sender, e);

            //movimiento 1
            desplazar(pilaPicBox1, pilaPicBox2, pictureBox5, panel2);
            //movimiento 2
            desplazar(pilaPicBox1, pilaPicBox3, pictureBox4, panel3);
            //movimiento 3
            desplazar(pilaPicBox2, pilaPicBox3, pictureBox5, panel3);
            //movimiento 4
            desplazar(pilaPicBox1, pilaPicBox2, pictureBox3, panel2);
            //movimiento 5
            desplazar(pilaPicBox3, pilaPicBox2, pictureBox5, panel2);
            //movimiento 6
            desplazar(pilaPicBox3, pilaPicBox1, pictureBox4, panel1);
            //movimiento 7
            desplazar(pilaPicBox2, pilaPicBox1, pictureBox5, panel1);
            //movimiento 8
            desplazar(pilaPicBox2, pilaPicBox3, pictureBox3, panel3);
            //movimiento 9
            desplazar(pilaPicBox1, pilaPicBox2, pictureBox5, panel2);
            //movimiento 10
            desplazar(pilaPicBox1, pilaPicBox3, pictureBox4, panel3);
            //movimiento 11
            desplazar(pilaPicBox2, pilaPicBox3, pictureBox5, panel3);
            MessageBox.Show(win);
        }

        private void desplazar(Stack<PictureBox> pila1, Stack<PictureBox> pila2, PictureBox pictureBox, Panel panel)
        {
            validador(pila1, pila2, pictureBox);
            pictureBox.Parent = panel;
            panel1.Update();
            panel2.Update();
            panel3.Update();
            pictureBox.BringToFront();
            pictureBox.Update();
            Thread.Sleep(500);
        }

        private void btnReiniciar_Click(object sender, EventArgs e)
        {
            TorresDeHanoi_Load(sender, e);
            pictureBox3.Top = panel1.Height - pictureBox1.Height - (1 * pictureBox1.Height);
            pictureBox4.Top = panel1.Height - pictureBox1.Height - (2 * pictureBox1.Height);
            pictureBox5.Top = panel1.Height - pictureBox1.Height - (3 * pictureBox1.Height);

        }
    }
}
