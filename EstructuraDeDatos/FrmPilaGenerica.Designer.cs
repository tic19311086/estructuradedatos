﻿namespace EstructuraDeDatos
{
    partial class FrmPilaGenerica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl10Elementos = new System.Windows.Forms.Label();
            this.lblPila = new System.Windows.Forms.Label();
            this.lblTope = new System.Windows.Forms.Label();
            this.lblLimiteIInferior = new System.Windows.Forms.Label();
            this.lblLimiteSuperior = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtInferior = new System.Windows.Forms.TextBox();
            this.txtSuperior = new System.Windows.Forms.TextBox();
            this.txtCaptura = new System.Windows.Forms.TextBox();
            this.btnReversa = new System.Windows.Forms.Button();
            this.btnRandom = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnContains = new System.Windows.Forms.Button();
            this.btnGet = new System.Windows.Forms.Button();
            this.btnPilita = new System.Windows.Forms.Button();
            this.btnPop = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl10Elementos
            // 
            this.lbl10Elementos.AutoSize = true;
            this.lbl10Elementos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl10Elementos.Location = new System.Drawing.Point(86, 88);
            this.lbl10Elementos.Name = "lbl10Elementos";
            this.lbl10Elementos.Size = new System.Drawing.Size(289, 24);
            this.lbl10Elementos.TabIndex = 0;
            this.lbl10Elementos.Text = "La pila contiene 10 elementos";
            this.lbl10Elementos.Click += new System.EventHandler(this.lbl10Elementos_Click);
            // 
            // lblPila
            // 
            this.lblPila.AutoSize = true;
            this.lblPila.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPila.Location = new System.Drawing.Point(464, 88);
            this.lblPila.Name = "lblPila";
            this.lblPila.Size = new System.Drawing.Size(248, 24);
            this.lblPila.TabIndex = 1;
            this.lblPila.Text = "En el tope de la pila hay: ";
            // 
            // lblTope
            // 
            this.lblTope.AutoSize = true;
            this.lblTope.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTope.Location = new System.Drawing.Point(12, 167);
            this.lblTope.Name = "lblTope";
            this.lblTope.Size = new System.Drawing.Size(59, 24);
            this.lblTope.TabIndex = 2;
            this.lblTope.Text = "Tope";
            // 
            // lblLimiteIInferior
            // 
            this.lblLimiteIInferior.AutoSize = true;
            this.lblLimiteIInferior.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLimiteIInferior.Location = new System.Drawing.Point(324, 189);
            this.lblLimiteIInferior.Name = "lblLimiteIInferior";
            this.lblLimiteIInferior.Size = new System.Drawing.Size(136, 24);
            this.lblLimiteIInferior.TabIndex = 3;
            this.lblLimiteIInferior.Text = "Limite Inferior";
            // 
            // lblLimiteSuperior
            // 
            this.lblLimiteSuperior.AutoSize = true;
            this.lblLimiteSuperior.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLimiteSuperior.Location = new System.Drawing.Point(324, 260);
            this.lblLimiteSuperior.Name = "lblLimiteSuperior";
            this.lblLimiteSuperior.Size = new System.Drawing.Size(151, 24);
            this.lblLimiteSuperior.TabIndex = 4;
            this.lblLimiteSuperior.Text = "Limite Superior";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.dataGridView1.Location = new System.Drawing.Point(90, 167);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(201, 365);
            this.dataGridView1.TabIndex = 5;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Pilas";
            this.Column1.Name = "Column1";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Desktop;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1026, 81);
            this.panel1.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(417, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pila Generica";
            // 
            // txtInferior
            // 
            this.txtInferior.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInferior.Location = new System.Drawing.Point(504, 189);
            this.txtInferior.Multiline = true;
            this.txtInferior.Name = "txtInferior";
            this.txtInferior.Size = new System.Drawing.Size(117, 30);
            this.txtInferior.TabIndex = 7;
            this.txtInferior.Text = "1";
            // 
            // txtSuperior
            // 
            this.txtSuperior.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSuperior.Location = new System.Drawing.Point(504, 257);
            this.txtSuperior.Multiline = true;
            this.txtSuperior.Name = "txtSuperior";
            this.txtSuperior.Size = new System.Drawing.Size(117, 33);
            this.txtSuperior.TabIndex = 8;
            this.txtSuperior.Text = "100";
            // 
            // txtCaptura
            // 
            this.txtCaptura.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCaptura.Location = new System.Drawing.Point(504, 388);
            this.txtCaptura.Multiline = true;
            this.txtCaptura.Name = "txtCaptura";
            this.txtCaptura.Size = new System.Drawing.Size(117, 30);
            this.txtCaptura.TabIndex = 9;
            this.txtCaptura.TextChanged += new System.EventHandler(this.txtCaptura_TextChanged);
            // 
            // btnReversa
            // 
            this.btnReversa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReversa.Location = new System.Drawing.Point(676, 257);
            this.btnReversa.Name = "btnReversa";
            this.btnReversa.Size = new System.Drawing.Size(124, 33);
            this.btnReversa.TabIndex = 10;
            this.btnReversa.Text = "pilita.Reverse()";
            this.btnReversa.UseVisualStyleBackColor = true;
            this.btnReversa.Click += new System.EventHandler(this.btnReversa_Click);
            // 
            // btnRandom
            // 
            this.btnRandom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRandom.Location = new System.Drawing.Point(328, 320);
            this.btnRandom.Name = "btnRandom";
            this.btnRandom.Size = new System.Drawing.Size(124, 33);
            this.btnRandom.TabIndex = 11;
            this.btnRandom.Text = "Random";
            this.btnRandom.UseVisualStyleBackColor = true;
            this.btnRandom.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Location = new System.Drawing.Point(468, 320);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(124, 33);
            this.btnLimpiar.TabIndex = 12;
            this.btnLimpiar.Text = "pilita.Clear()";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnContains
            // 
            this.btnContains.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnContains.Location = new System.Drawing.Point(611, 320);
            this.btnContains.Name = "btnContains";
            this.btnContains.Size = new System.Drawing.Size(124, 33);
            this.btnContains.TabIndex = 13;
            this.btnContains.Text = "pilita.Contains()";
            this.btnContains.UseVisualStyleBackColor = true;
            this.btnContains.Click += new System.EventHandler(this.btnContains_Click);
            // 
            // btnGet
            // 
            this.btnGet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGet.Location = new System.Drawing.Point(759, 320);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(124, 33);
            this.btnGet.TabIndex = 14;
            this.btnGet.Text = "pilita.getType()";
            this.btnGet.UseVisualStyleBackColor = true;
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // btnPilita
            // 
            this.btnPilita.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPilita.Location = new System.Drawing.Point(328, 388);
            this.btnPilita.Name = "btnPilita";
            this.btnPilita.Size = new System.Drawing.Size(124, 33);
            this.btnPilita.TabIndex = 15;
            this.btnPilita.Text = "pilita.Push()";
            this.btnPilita.UseVisualStyleBackColor = true;
            this.btnPilita.Click += new System.EventHandler(this.btnPilita_Click);
            // 
            // btnPop
            // 
            this.btnPop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPop.Location = new System.Drawing.Point(328, 467);
            this.btnPop.Name = "btnPop";
            this.btnPop.Size = new System.Drawing.Size(124, 33);
            this.btnPop.TabIndex = 16;
            this.btnPop.Text = "pilita.Pop";
            this.btnPop.UseVisualStyleBackColor = true;
            this.btnPop.Click += new System.EventHandler(this.btnPop_Click);
            // 
            // FrmPilaGenerica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1020, 557);
            this.Controls.Add(this.btnPop);
            this.Controls.Add(this.btnPilita);
            this.Controls.Add(this.btnGet);
            this.Controls.Add(this.btnContains);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnRandom);
            this.Controls.Add(this.btnReversa);
            this.Controls.Add(this.txtCaptura);
            this.Controls.Add(this.txtSuperior);
            this.Controls.Add(this.txtInferior);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblLimiteSuperior);
            this.Controls.Add(this.lblLimiteIInferior);
            this.Controls.Add(this.lblTope);
            this.Controls.Add(this.lblPila);
            this.Controls.Add(this.lbl10Elementos);
            this.Name = "FrmPilaGenerica";
            this.Text = "FrmPilaGenerica";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl10Elementos;
        private System.Windows.Forms.Label lblPila;
        private System.Windows.Forms.Label lblTope;
        private System.Windows.Forms.Label lblLimiteIInferior;
        private System.Windows.Forms.Label lblLimiteSuperior;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInferior;
        private System.Windows.Forms.TextBox txtSuperior;
        private System.Windows.Forms.TextBox txtCaptura;
        private System.Windows.Forms.Button btnReversa;
        private System.Windows.Forms.Button btnRandom;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnContains;
        private System.Windows.Forms.Button btnGet;
        private System.Windows.Forms.Button btnPilita;
        private System.Windows.Forms.Button btnPop;
    }
}